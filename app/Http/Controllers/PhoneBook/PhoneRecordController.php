<?php

namespace App\Http\Controllers\PhoneBook;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PhoneBook;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PhoneRecordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // using paginate function to show 10 records per page
        $itemsPerPage = 10;
        $record = PhoneBook::orderBy('created_at', 'desc')->paginate($itemsPerPage);
        return view('phonerecord.index', array('phonerecordlist' => $record, 'title' => 'Record Display'));
    }

    /**
     * Store record into db.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:50',
                'phone' => 'required|numeric',
                'address' => 'required|string|max:255',
            )
        );
        $input = $request->all();
        $input['user_id'] = Auth::id();
        PhoneBook::create($input);
        Session::flash('flash_message', 'Book Record added successfully!');
        return redirect()->back();
    }

    /**
     * Show the form for creating a new record.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('phonerecord.create', array('title' => 'Add New PhoneBook Record'));
    }

    /**
     * Remove the specific record from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = PhoneBook::findOrFail($id);
        $news->delete();
        Session::flash('flash_message', 'Record deleted successfully!');
        return redirect()->route('phonerecord.view');
    }

    /**
     * Display list of deleted records
     *
     * @return \Illuminate\Http\Response
     */
    public function restore()
    {
        //$news = News::all();

        // using paginate function to show 10 records per page
        $itemsPerPage = 10;
        $record = PhoneBook::onlyTrashed()->orderBy('created_at', 'desc')->paginate($itemsPerPage);

        return view('phonerecord.restore', array('phonerecordlist' => $record, 'title' => 'Display Deleted Record'));
    }

    /**
     * restore deleted record
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restored($id)
    {
        $news = PhoneBook::withTrashed()
            ->where('id', $id)
            ->restore();
        Session::flash('flash_message', 'Record restored successfully!');
        return redirect()->route('phonerecord.restore');
    }

    /**
     * Edit record
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = PhoneBook::findOrFail($id);
        return view('phonerecord.edit', array('record' => $record, 'title' => 'Edit Book Record'));
    }

    /**
     * Update the specified record in db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $record = PhoneBook::findOrFail($id);
        $this->validate($request, array(
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:50',
                'phone' => 'required|numeric',
                'address' => 'required|string|max:255',
            )
        );

        $input = $request->all();
        $record->fill($input)->save();
        Session::flash('flash_message', 'Record updated successfully!');
        return redirect()->back();
    }
}
