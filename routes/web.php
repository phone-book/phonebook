<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// before
Auth::routes();

// after
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('phonerecord/view', array('as' => 'phonerecord.view', 'uses' => 'PhoneBook\PhoneRecordController@index'))->middleware('verified');
Route::get('phonerecord/add', array('as' => 'phonerecord.create', 'uses' => 'PhoneBook\PhoneRecordController@create'))->middleware('verified');
Route::post('phonerecord/store', array('as' => 'phonerecord.store', 'uses' => 'PhoneBook\PhoneRecordController@store'))->middleware('verified');
Route::get('phonerecord/edit/{id}', array('as' => 'phonerecord.edit', 'uses' => 'PhoneBook\PhoneRecordController@edit'))->middleware('verified');
Route::patch('phonerecord/update/{id}', array('as' => 'phonerecord.update', 'uses' => 'PhoneBook\PhoneRecordController@update'))->middleware('verified');
Route::delete('phonerecord/delete/{id}', array('as' => 'phonerecord.destroy', 'uses' => 'PhoneBook\PhoneRecordController@destroy'))->middleware('verified');
Route::get('phonerecord/restore', array('as' => 'phonerecord.restore', 'uses' => 'PhoneBook\PhoneRecordController@restore'))->middleware('verified');
Route::get('phonerecord/restored/{id}', array('as' => 'phonerecord.restored', 'uses' => 'PhoneBook\PhoneRecordController@restored'))->middleware('verified');
