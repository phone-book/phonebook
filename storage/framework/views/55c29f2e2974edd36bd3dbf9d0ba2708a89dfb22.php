<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><?php echo e($title); ?></div>
                <?php if(Session::has('flash_message')): ?>
                    <div style="color:green; border:1px solid #aaa; padding:4px; margin-top:10px">
                        <?php echo e(Session::get('flash_message')); ?>

                    </div>
                <?php endif; ?>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $phonerecordlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($data->first_name); ?></td>
                                    <td><?php echo e($data->last_name); ?></td>
                                    <td><?php echo e($data->email); ?></td>
                                    <td><?php echo e($data->phone); ?></td>
                                    <td><?php echo e($data->address); ?></td>
                                    <td>
                                        <?php echo Form::open(array(
                                                'method' => 'DELETE',
                                                'route' => ['phonerecord.destroy', $data->id],
                                                'onsubmit' => "return confirm('Are you sure you want to delete?')",
                                            )); ?>

                                        <?php echo Form::submit('Delete'); ?>

                                        <?php echo Form::close(); ?>

                                        <a href="<?php echo e(route('phonerecord.edit',$data->id)); ?>" class="btn btn-primary">Edit</a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>