@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$title}}</div>
                @if(Session::has('flash_message'))
                    <div style="color:green; border:1px solid #aaa; padding:4px; margin-top:10px">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($phonerecordlist as $data)
                                <tr>
                                    <td>{{$data->first_name}}</td>
                                    <td>{{$data->last_name}}</td>
                                    <td>{{$data->email}}</td>
                                    <td>{{$data->phone}}</td>
                                    <td>{{$data->address}}</td>
                                    <td>
                                        {!! Form::open(array(
                                                'method' => 'GET',
                                                'route' => ['phonerecord.restored', $data->id],
                                                'onsubmit' => "return confirm('Are you sure you want to restore?')",
                                            ))
                                        !!}
                                        {!! Form::submit('Restore') !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
